import "../scss/app.scss";

document.body.addEventListener("click", function() {
  for (let i = 0; i < 5; i++) {
    let message = document.createElement("article");
    message.classList.add("message");
    message.innerHTML = "This is message " + (i + 1);
    document.body.appendChild(message);
  }
});
